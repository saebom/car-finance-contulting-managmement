package com.example.carfinancecontultingmanagement.model;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.validator.constraints.Length;

import javax.validation.constraints.NotNull;

@Getter
@Setter
public class CarManufacturerRequest {

    @NotNull
    @Length(min = 2, max = 30)
    private String manufacturerName; //제조사이름

    @NotNull
    @Length(min = 13, max = 30)
    private String manufacturerPhone; //제조사번호

}
