package com.example.carfinancecontultingmanagement.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarOption {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carRatingId", nullable = false)
    private CarRating carRating; //차등급

    @Column(nullable = false, length = 20)
    private String engineType; //엔진타입

    @Column(nullable = false)
    private Integer displacement; //배기량

    @Column(nullable = false)
    private Integer fuelEconomyRating; //연비등급

    @Column(nullable = false)
    private Double highwayFuelEfficiency; //고속도로연비

    @Column(nullable = false)
    private Integer personnel; //인원

    @Column(nullable = false, length = 10)
    private String transmission; //변속기

    @Column(nullable = false, length = 10)
    private String driveMethod; //구동방식

    @Column(nullable = false)
    private Integer highestOutput; //최고출력

    @Column(nullable = false)
    private Double maximumTorque; //최대토크

    private Double cityFuelEfficiency; //도심연비
}
