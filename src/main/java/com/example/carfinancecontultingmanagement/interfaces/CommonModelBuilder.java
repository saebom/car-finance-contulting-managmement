package com.example.carfinancecontultingmanagement.interfaces;

public interface CommonModelBuilder<T> {
    T build();
}
