package com.example.carfinancecontultingmanagement.repository;

import com.example.carfinancecontultingmanagement.entity.CarManufacturer;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CarManufacturerRepository extends JpaRepository<CarManufacturer, Long> {
}
