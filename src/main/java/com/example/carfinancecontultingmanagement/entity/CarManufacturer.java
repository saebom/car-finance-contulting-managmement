package com.example.carfinancecontultingmanagement.entity;

import com.example.carfinancecontultingmanagement.interfaces.CommonModelBuilder;
import com.example.carfinancecontultingmanagement.model.CarManufacturerRequest;
import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarManufacturer {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 30)
    private String manufacturerName; //제조사이름

    @Column(nullable = false, length = 30)
    private String manufacturerPhone; //제조사번호

    private CarManufacturer(CarManufacturerBuilder builder) {
        this.manufacturerName = builder.manufacturerName;
        this.manufacturerPhone = builder.manufacturerPhone;
    }

    public static class CarManufacturerBuilder implements CommonModelBuilder<CarManufacturer> {
        private final String manufacturerName; //제조사이름
        private final String manufacturerPhone; //제조사번호

        public CarManufacturerBuilder (CarManufacturerRequest request) {
            this.manufacturerName = request.getManufacturerName();
            this.manufacturerPhone = request.getManufacturerPhone();
        }

        @Override
        public CarManufacturer build() {
            return new CarManufacturer(this);
        }
    }
}
