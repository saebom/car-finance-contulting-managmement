package com.example.carfinancecontultingmanagement.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import java.util.StringTokenizer;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarRating {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(nullable = false, length = 10)
    private String fuelType;  //차량 연료 타입


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "carModelId", nullable = false)
    private CarModel carModel;
}
