package com.example.carfinancecontultingmanagement.service;

import com.example.carfinancecontultingmanagement.entity.CarManufacturer;
import com.example.carfinancecontultingmanagement.model.CarManufacturerRequest;
import com.example.carfinancecontultingmanagement.repository.CarManufacturerRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class CarManufacturerService {
    private  final CarManufacturerRepository carManufacturerRepository;

    public void setCarManufacturer(CarManufacturerRequest request) {
        CarManufacturer carManufacturer = new CarManufacturer.CarManufacturerBuilder(request).build();
        carManufacturerRepository.save(carManufacturer);
    }
}
