package com.example.carfinancecontultingmanagement.entity;

import lombok.AccessLevel;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.*;

@Entity
@Getter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
public class CarModel {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

   @Column(nullable = false, length = 30)
    private String modelName;

    @Column(nullable = false, length = 10)
    private String modelType; //모델 타입

    @Column(nullable = false, length = 20)
    private String modelAppearance;  //모델외관

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "CarManuacturerId", nullable = false)
    private CarManufacturer carManufacturer;

}
