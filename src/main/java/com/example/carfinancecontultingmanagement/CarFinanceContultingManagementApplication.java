package com.example.carfinancecontultingmanagement;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CarFinanceContultingManagementApplication {

    public static void main(String[] args) {
        SpringApplication.run(CarFinanceContultingManagementApplication.class, args);
    }

}
